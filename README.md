# Описание приложения task-manager

## Приложение:        task-manager
## Требование:        Управление списком задач
## Стек технологий:   java
## Разработчик:       sobolev_mv
## e-mail:            sobolev_mv@nlmk.com

### Сборка приложения: mvn clean, mvn package, mvn install 
### Запуск приложения: java -jar task-manager-1.0.0.jar